# PRCToken-Account

## build
1.   ./autogen.sh
2.   mkdir build
3.   cd build
4.   ../configure 
5.   make


## Run
* Start tendermint
tendermint init
tendermint node

* Start PRCToken ABCI-Server
cd build
./src/PRCToken

## Make Key
cd build/src
./kstest

## Send Tx 
./txcli --key_path ./key.json --recv_addr FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF --recv_amot 333

Arguments:
1. --key_path  <Your Key File>
2. --recv_addr <Receive Account address>
3. --recv_amot <Send token num>



## Test Case
* Crypto Test
cd build/src
./kstest

* KV-Store Test
./kstest



