#ifndef _PEAR_DUMP_H_
#define _PEAR_DUMP_H_

char *pr_hex_encode(char *, unsigned char *, int);
char *pr_hex_decode(unsigned char *, const char *, int);

#endif

