#include <string.h>
#include "log/pear_log.h"
#include "jansson.h"
#include "pear_json.h"


int pr_json_foreach_object(json_t *obj, const char *key, char *value)
{
    PEAR_LOG("[pr-router] pr_json_foreach_object start");
    json_t *_value;
    json_t *temp_obj;
    const char *_key;

    json_object_foreach(obj, _key, _value)
    {
        temp_obj = json_object_get(obj, _key);

        if (json_is_object(temp_obj))
        {
            PEAR_LOG("[pr-router] is object");
            if (pr_json_foreach_object(temp_obj, key, value) > 0)
            {
                return 1;
            }
        }
        else if (json_is_array(temp_obj))
        {
            PEAR_LOG("[pr-router] is array");
            if (pr_json_foreach_array(temp_obj, key, value) > 0)
            {
                return 1;
            }
        }
        else if (json_is_string(temp_obj))
        {
            PEAR_LOG("[pr-router] is string");
            if (strcmp(key, _key) == 0)
            {
                PEAR_LOG("[pr-router] find it");
                strcpy(value, json_string_value(_value));
                PEAR_LOG("[pr-router] copy it");
                return 1;
            }
        }
        else
        {
            PEAR_LOG("[pr-router] unknow type");
        }
    }

    return -1;
}

int pr_json_foreach_array(json_t *obj, const char *key, char *value)
{
    PEAR_LOG("pr_json_foreach_array start");
    size_t index     = 0;
    json_t *_value   = NULL;
    json_t *temp_obj = NULL;
    const char *_key = NULL;

    json_array_foreach(obj, index, _value)
    {
        temp_obj = json_array_get(obj, index);

        if (json_is_object(temp_obj))
        {
            if (pr_json_foreach_object(temp_obj, key, value) > 0)
            {
                return 1;
            }
        }
        else if (json_is_array(obj))
        {
            if (pr_json_foreach_array(temp_obj, key, value) > 0)
            {
                return 1;
            }
        }
        else if (json_is_string(temp_obj))
        {
            if (strcmp(key, _key) == 0)
            {
                PEAR_LOG("[pr-router] array find it");
                strcpy(value, json_string_value(_value));
                PEAR_LOG("[pr-router] array copy it");
                return 1;
            }
        }
    }

    return -1;
}

int pr_json_get_string(char *buf, char *key, char *myvalue)
{
    PEAR_LOG("pr_json_get_string start");
    json_error_t error;
    json_t *obj = NULL;

    obj = json_loads(buf, 0, &error);
    if (obj == NULL)
    {
        PEAR_LOG("[ss-json] json_loads error!! json_string:%s", buf);
        PEAR_LOG("[ss-json] json_error_code: %s", error.text);
    }

    if (pr_json_foreach_object(obj, key, myvalue) > 0)
    {
        json_decref(obj);
        return 1;
    }
    else
    {
        PEAR_LOG("[pr-router] Invalid config string");
    }

    json_decref(obj);

    return -1;
}

void pr_json_get_server(char *buf, char *server, char *server_port, char *password)
{
    PEAR_LOG("pr_json__get_server start");
    json_error_t error;
    json_t *obj = NULL;

    obj = json_loads(buf, 0, &error);
    if (!pr_json_foreach_object(obj, "server", server))
    {
        PEAR_LOG("[pr-router] Invalid config string");
    }

    if (!pr_json_foreach_object(obj, "server_port", server_port))
    {
        PEAR_LOG("[pr-router] Invalid config string");
    }

    if (!pr_json_foreach_object(obj, "password", password))
    {
        PEAR_LOG("[pr-router] Invalid config string");
    }

    json_decref(obj);
}

int pr_json_get_string_file(char *path, char *key, char *myvalue)
{
    PEAR_LOG("pr_json_get_string start");
    json_error_t error;
    json_t *obj = NULL;

    obj = json_load_file(path, 0, &error);
    if (obj == NULL)
    {
        PEAR_LOG("[ss-json] json_loads error!! json_file:%s", path);
        PEAR_LOG("[ss-json] json_error_code: %s", error.text);
    }

    if (pr_json_foreach_object(obj, key, myvalue) > 0)
    {
        json_decref(obj);
        return 1;
    }
    else
    {
        PEAR_LOG("[pr-router] Invalid config string");
    }

    json_decref(obj);

    return -1;
}
