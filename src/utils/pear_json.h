#ifndef _PEAR_JSON_H
#define _PEAR_JSON_H

#include "jansson.h"

int  pr_json_get_string(char *buf, char *key, char *myvalue);
int  pr_json_get_string_file(char *path, char *key, char *myvalue);
int  pr_json_foreach_object(json_t *obj, const char *key, char *value);
int  pr_json_foreach_array(json_t *obj,  const char *key, char *value);
void pr_json_get_server(char *buf, char *server, char *server_port, char *password);

#endif
