/*
 * Copyright (C) 2018 - 2019, Jue Huang <rabbit@pear.hk>
 *
 * Binary to Hex-String Encode
 * Hex-String to Binary Decode
 *
 */

#include <stdio.h>

#include "pear_dump.h"
#include "log/pear_log.h"

char *pr_hex_encode(char *binstr, unsigned char *bin, int len)
{
    int i = 0;

    for (; i < len; i++)
    {
        sprintf(binstr + i * 2, "%02X", bin[i]);
    }

    binstr[i * 2] = 0;

    return binstr;
}

char *pr_hex_decode(unsigned char *out, const char *in, int len)
{
        unsigned int i, t, hn, ln;

        for (t = 0,i = 0; i < len; i += 2, t++)
        {
                hn = in[i]   > '9' ? (in[i] - 'A' + 10)     : (in[i] - '0');

                ln = in[i+1] > '9' ? (in[i + 1] - 'A' + 10) : (in[i + 1] - '0');

                out[t] = (hn << 4 ) | ln;
        }

        return out;
}
