#ifndef __PEAR_CURL_H_
#define __PEAR_CURL_H_

#include "stdio.h"
#include <curl/curl.h>

long pr_curl_cmd(char *method, char *url,  char *par, char *header[], char *body, long body_size, char *recvbuf);

long pr_curl_cmd_debug(char *method, char *url,  char *par, char *header[], char *body, long body_size, char *recvbuf);


#endif


