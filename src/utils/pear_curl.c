#include <stdlib.h>
#include <memory.h>
#include "pear_curl.h"


//fuqiyang
//
static void pr_dump(const char *text, FILE *stream, unsigned char *ptr, size_t size)
{
   size_t i;
   size_t c;
   unsigned int width = 0x10;

   fprintf(stream, "%s, %10.10ld bytes (0x%8.8lx)\n", text, (long)size, (long)size);

   for(i=0; i<size; i+= width) {

       fprintf(stream, "%4.4lx: ", (long)i);

       /* show hex to the left */
       for(c = 0; c < width; c++) {
           if(i+c < size)
               fprintf(stream, "%02x ", ptr[i+c]);
           else
               fputs("   ", stream);
       }

       /* show data on the right */
       for(c = 0; (c < width) && (i+c < size); c++) {
           char x = (ptr[i+c] >= 0x20 && ptr[i+c] < 0x80) ? ptr[i+c] : '.';
           fputc(x, stream);
       }

       fputc('\n', stream); /* newline */
   }
}


// fuqiyang
//
static int pr_curl_trace(CURL *handle, curl_infotype type, char *data, size_t size, void *userp)
{
   const char *text;

   (void)handle; /* prevent compiler warning */

   (void)userp;

   switch (type) {
       case CURLINFO_TEXT:
           fprintf(stderr, "== Info: %s", data);
       default: /* in case a new one is introduced to shock us */
           return 0;

       case CURLINFO_HEADER_OUT:
           text = "=> Send header";
           break;
       case CURLINFO_DATA_OUT:
           text = "=> Send data";
           break;
       case CURLINFO_SSL_DATA_OUT:
           text = "=> Send SSL data";
           break;
       case CURLINFO_HEADER_IN:
           text = "<= Recv header";
           break;
       case CURLINFO_DATA_IN:
           text = "<= Recv data";
           break;
       case CURLINFO_SSL_DATA_IN:
           text = "<= Recv SSL data";
           break;
   }

   pr_dump(text, stderr, (unsigned char *)data, size);
   return 0;
}


struct Pear_Curl_Buffer {

    char *buffer;

    size_t size;
};


static size_t pr_curl_writecb(void *buf, size_t size, size_t nmemb, void *recv)
{
    size_t realsize = size * nmemb;

    struct Pear_Curl_Buffer *chunk = (struct Pear_Curl_Buffer *)recv;

    chunk->buffer = realloc(chunk->buffer, chunk->size + realsize + 1);

    if (NULL == chunk->buffer) {

        chunk->size = 0;

        return 0;
    }

    memcpy(&(chunk->buffer[chunk->size]), buf, realsize);

    chunk->size += realsize;

    chunk->buffer[chunk->size] = 0;

    return realsize;
}



// fuqiyang  2017-03-27
//
long pr_curl_cmd(char *method, char *url,  char *par, char *header[], char *body, long body_size, char *recvbuf)
{
    curl_global_init(CURL_GLOBAL_ALL);

    CURL *curl_handle = curl_easy_init();

    /// for debug
    ///
    //curl_easy_setopt(curl_handle, CURLOPT_DEBUGFUNCTION, pr_curl_trace);
    //curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);

    /// add header
    ///
    struct curl_slist *curl_headers = NULL;
    
    if (header != NULL) {

        for (; *header; header++) {
            curl_headers = curl_slist_append(curl_headers, *header);
        }
        curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, curl_headers);
    }

    /// set callback function
    ///
    struct Pear_Curl_Buffer chunk;

    chunk.buffer = malloc(1);

    chunk.size = 0;

    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, pr_curl_writecb);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, &chunk);

    /// POST and GET, decide if there is body data
    ///
    if (strcmp(method, "POST") == 0) {

        curl_easy_setopt(curl_handle, CURLOPT_POST, 1L);

        curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDSIZE, body_size);

        curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDS, body);

    } else if (strcmp(method, "GET") == 0) {

    } else { return -1; }

    /// other options
    ///
    curl_easy_setopt(curl_handle, CURLOPT_NOBODY, 0L);
    curl_easy_setopt(curl_handle, CURLOPT_TIMEOUT, 30);
    curl_easy_setopt(curl_handle, CURLOPT_URL, url);
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl");
    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0);

    /// perform the url
    ///
    curl_easy_perform(curl_handle);

    long retcode;

    CURLcode res = curl_easy_getinfo(curl_handle, CURLINFO_HTTP_CODE, &retcode);

    /// copy data
    ///
    memcpy(recvbuf, chunk.buffer, chunk.size);

    /// clean up
    ///
    free(chunk.buffer);

    curl_slist_free_all(curl_headers);

    curl_easy_cleanup(curl_handle);

    curl_global_cleanup();

    return retcode;
}



long pr_curl_cmd_debug(char *method, char *url,  char *par, char *header[], char *body, long body_size, char *recvbuf)
{
    curl_global_init(CURL_GLOBAL_ALL);

    CURL *curl_handle = curl_easy_init();

    /// for debug
    ///
    curl_easy_setopt(curl_handle, CURLOPT_DEBUGFUNCTION, pr_curl_trace);
    curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);

    /// add header
    ///
    struct curl_slist *curl_headers = NULL;
    
    if (header != NULL) {

        for (; *header; header++) {
            curl_headers = curl_slist_append(curl_headers, *header);
        }
        curl_easy_setopt(curl_handle, CURLOPT_HTTPHEADER, curl_headers);
    }

    /// set callback function
    ///
    struct Pear_Curl_Buffer chunk;

    chunk.buffer = malloc(1);

    chunk.size = 0;

    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, pr_curl_writecb);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, &chunk);

    /// POST and GET, decide if there is body data
    ///
    if (strcmp(method, "POST") == 0) {

        curl_easy_setopt(curl_handle, CURLOPT_POST, 1L);

        curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDSIZE, body_size);

        curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDS, body);

    } else if (strcmp(method, "GET") == 0) {

    } else { return -1; }

    /// other options
    ///
    curl_easy_setopt(curl_handle, CURLOPT_NOBODY, 0L);
    curl_easy_setopt(curl_handle, CURLOPT_TIMEOUT, 30);
    curl_easy_setopt(curl_handle, CURLOPT_URL, url);
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl");
    curl_easy_setopt(curl_handle, CURLOPT_SSL_VERIFYPEER, 0);

    /// perform the url
    ///
    curl_easy_perform(curl_handle);

    long retcode;

    CURLcode res = curl_easy_getinfo(curl_handle, CURLINFO_HTTP_CODE, &retcode);

    /// copy data
    ///
    memcpy(recvbuf, chunk.buffer, chunk.size);

    /// clean up
    ///
    free(chunk.buffer);

    curl_slist_free_all(curl_headers);

    curl_easy_cleanup(curl_handle);

    curl_global_cleanup();

    return retcode;
}




