#ifndef _PEAR_ACCOUNT_H_
#define _PEAR_ACCOUNT_H_

#include "pear_coin.h"
#include "crypto/pear_key.h"
#include "store/pear_kv_store.h"

typedef struct pr_account_s pr_account_t;

struct pr_account_s {
    uint64_t    sequence;
    pr_coin_t   balance;
    pr_pubkey_t pubkey;
};


typedef struct pr_acc_ctx_s pr_acc_ctx_t;
struct pr_acc_ctx_s {
    pr_kv_store_t *store;
};

pr_acc_ctx_t *pr_new_acc_ctx();
pr_account_t *pr_add_account(pr_acc_ctx_t *, char *path, pr_coin_t balance);
pr_account_t *pr_get_account(pr_acc_ctx_t *, char *addr);
pr_account_t *pr_set_account(pr_acc_ctx_t *, char *addr, pr_account_t *acc);

int pr_account_query(pr_acc_ctx_t *, char *addr);

#endif

