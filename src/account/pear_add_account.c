#include <stdio.h>
#include <stdlib.h>
#include "pear_account.h"

/*
 * Example: ./new_account
 *
 */
void usage()
{
    printf("Usage:"
           "./txcli path balance");
}

int main(int argc, char *argv[])
{
    pr_coin_t     balance = 0;
    pr_acc_ctx_t *acc_ctx = NULL;
    /*
     * argv[1]: key_apth
     */
    if (argc != 3)
    {
        printf("arguments error\n");
        usage();
        exit(-1);
    }

    acc_ctx = pr_new_acc_ctx();
    if (acc_ctx == NULL)
    {
        printf("pr_new_acc_ctx error\n");
        exit(-1);
    }

    balance = (pr_coin_t) atoi(argv[2]);
    if(pr_add_account(acc_ctx, argv[1], balance) == NULL)
    {
        printf("add account error\n");
        exit(-1);
    }

    printf("add account success!\n");

    return 0;
}

