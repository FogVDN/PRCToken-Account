#ifndef _PEAR_TX_H
#define _PEAR_TX_H

#include "account/pear_account.h"

typedef int (*pr_check_tx_p)(pr_acc_ctx_t *,   void *, int);
typedef int (*pr_deliver_tx_p)(pr_acc_ctx_t *, void *, int);

typedef struct pr_tx_ctx_s pr_tx_ctx_t;

struct pr_tx_ctx_s {
    pr_check_tx_p   check_tx;
    pr_deliver_tx_p deliver_tx;
};

pr_tx_ctx_t *pr_new_tx_ctx(char);

#endif
