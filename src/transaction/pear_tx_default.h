#ifndef _PEAR_TRANSACTION_H_
#define _PEAR_TRANSACTION_H_


#include "pear_tx_ctx.h"
#include "crypto/pear_key.h"
#include "account/pear_addr.h"
#include "account/pear_coin.h"

typedef struct pr_tx_in_s pr_tx_in_t;

struct pr_tx_in_s {
    pr_addr_t    from;
    pr_pubkey_t  key;
    pr_coin_t    amount;
    pr_sign_t    sign;
    int          sequence;
};

typedef struct pr_tx_out_s pr_tx_out_t;

struct pr_tx_out_s {
    pr_addr_t   to;
    pr_coin_t   amount;
};

typedef struct pr_tx_s pr_tx_t;

struct pr_tx_s {
    int         gas;
    pr_coin_t   fee;
    pr_tx_in_t  input;
    pr_tx_out_t output;
};

typedef struct pr_tx_default_s pr_tx_default_t;
struct pr_tx_default_s {
    pr_tx_ctx_t tx_ctx;
};

pr_tx_default_t *pr_new_tx_default();

#endif
