#include <stdlib.h>

#include "pear_tx_ctx.h"
#include "log/pear_log.h"
#include "pear_tx_default.h"

pr_tx_ctx_t *pr_new_tx_ctx(char type)
{
    pr_tx_ctx_t *tx_ctx = NULL;

/*
    tx = (pr_tx_t *) malloc(sizeof(*tx));
    if (tx == NULL)
    {
        PEAR_LOG("malloc pr_tx_t error\n");
        return NULL;
    }
*/

    switch(type)
    {
        case 0:
            tx_ctx = (pr_tx_ctx_t *) pr_new_tx_default();
            break;

        case 1:
            tx_ctx = NULL;
            break;

        default:
            PEAR_LOG("Unknow Tx Type\n");
    }

    return tx_ctx;
}
