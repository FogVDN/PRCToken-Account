#include <stdlib.h>
#include <string.h>

#include "pear_base64.h"
#include "log/pear_log.h"
#include "pear_tx_default.h"
#include "pear_tx_default.h"

char *pr_tx_encode(pr_tx_t *tx)
{
    int   len   = 0;
    char *txstr = NULL;

    len   = (sizeof(pr_tx_t) * 8 / 6) + (sizeof(pr_tx_t) *8 % 6);
    txstr = (char *) malloc(len);

    pr_Base64encode(txstr, (const char *)tx, sizeof(pr_tx_t));

    return txstr;
}

pr_tx_t *pr_tx_decode(void *txstr, int tx_len)
{
    pr_tx_t *tx = NULL;

    if (tx_len != sizeof(pr_tx_t))
    {
        PEAR_LOG("Decode Tx Length Error\n");
        return NULL;
    }

    tx = (pr_tx_t *) malloc(sizeof(pr_tx_t));
    if (tx == NULL)
    {
        PEAR_LOG("malloc pr_tx_t error\n");
        return NULL;
    }

    memset(tx, 0x00, sizeof(*tx));
    /* pr_Base64decode((char *)tx, txstr); */
    memcpy(tx, txstr, tx_len);

    return tx;
}

