#ifndef _PEAR_TX_SERIAL_H_
#define _PEAR_TX_SERIAL_H_

#include "pear_tx_default.h"

char *pr_tx_encode(pr_tx_t *);
pr_tx_t *pr_tx_decode(void *, int tx_len);

#endif
