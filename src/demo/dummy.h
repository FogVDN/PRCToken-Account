#ifndef _DUMMY_H_
#define _DUMMY_H_

#include "abci/abci.h"
#include <string.h>

void *ABCIApplication(Types__Request *request);

#endif
