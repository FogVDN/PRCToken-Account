#ifndef _PEAR_PRCTOKEN_H_
#define _PEAR_PRCTOKEN_H_

#include "abci/abci.h"
#include "account/pear_account.h"
#include "block/pear_block_ctx.h"
#include "transaction/pear_tx_ctx.h"
#include "validator/pear_validator.h"

typedef struct PRCToken_s PRCToken_t;

struct PRCToken_s {
    Application     app;
    pr_tx_ctx_t    *tx_ctx;
    pr_acc_ctx_t   *acc_ctx;
    pr_block_ctx_t *blk_ctx;
    pr_validator_ctx_t *valid_ctx;
};

PRCToken_t *pr_new_PRCToken();

#endif
