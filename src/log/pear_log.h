#ifndef _PEAR_LOG_H_
#define _PEAR_LOG_H_

#include <stdio.h>
#define PEAR_LOG(format, ...) printf(format, ##__VA_ARGS__);

#endif
