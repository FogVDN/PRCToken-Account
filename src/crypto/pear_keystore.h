#ifndef __PEAR_KEYSTORE_H__
#define __PEAR_KEYSTORE_H__

#include "pear_key.h"
int pr_set_keystore(pr_key_t *, char *);
pr_key_t *pr_get_keystore(char *);

#endif

