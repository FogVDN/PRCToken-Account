/*
 * Copyright (C) 2018 - 2019, Jue Huang <rabbit@pear.hk>
 *
 * Encryption Module Test Program
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pear_key.h"
#include "pear_keystore.h"
#include "utils/pear_dump.h"

int main()
{
    char hash[32]  = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    char buf[1024]  = {0x00};
    pr_key_t  *key  = NULL;
    pr_sign_t  sign;
    pr_compact_sign_t  c_sign;


    printf("\n#### KeyStore Test Start... ####\n");

    if ((key = pr_new_key()) == NULL)
    {
        printf("new key error\n");
        return -1;
    }

    if (pr_set_keystore(key, "./key.json") < 0)
    {
        printf("set keystore error\n");
        return -1;
    }

    if ((key = pr_get_keystore("./key.json")) == NULL)
    {
        printf("get keystore error\n");
        return -1;
    }

    if (pr_set_keystore(key, "./key.json.dump") < 0)
    {
        printf("set keystore dump error\n");
        return -1;
    }

    printf("\n#### KeyStore Test End... ####\n\n");



    printf("\n#### Sign Test Start... ####\n");

    if (pr_sign(hash, sizeof(hash), &sign, &(key->privkey)) == NULL)
    {
        printf("sign error\n");
        return -1;
    }

    printf("sign: %s\n", pr_hex_encode(buf, (unsigned char *)&(sign.data), SIGNATURE_SIZE));
    if (pr_verify(hash, sizeof(hash), &sign, &(key->pubkey)) < 0)
    {
        printf("verify failed\n");
        return -1;
    }
    printf("verify ok\n");
    printf("\n#### Sign Test End... ####\n\n");



    printf("\n#### Compact Sign Test Start... ####\n");

    if (pr_compact_sign(hash, sizeof(hash), &c_sign, &(key->privkey)) == NULL)
    {
        printf("sign error\n");
        return -1;
    }

    printf("c_sign: %s\n", pr_hex_encode(buf, (unsigned char *)&(c_sign.data), COMPACT_SIGNATURE_SIZE));
    if (pr_compact_verify(hash, sizeof(hash), &c_sign, &(key->pubkey)) < 0)
    {
        printf("verify failed\n");
        return -1;
    }
    printf("verify ok\n");
    printf("\n#### Compact Sign Test End... ####\n\n");



    return 0;
}
