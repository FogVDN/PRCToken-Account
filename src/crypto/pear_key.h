#ifndef __PEAR_KEY_H__
#define __PEAR_KEY_H__


#define PUBLIC_KEY_SIZE        65  /* Length of PublicKey is 65  */
#define PRIVATE_KEY_SIZE       279 /* Length of PrivKey   is 279 */
#define SIGNATURE_SIZE         72  /* Length of Signature is 72  */
#define COMPACT_SIGNATURE_SIZE 65  /* Length of Signature is 65  */

typedef unsigned char pr_pubkey_t[PUBLIC_KEY_SIZE];
typedef unsigned char pr_privkey_t[PRIVATE_KEY_SIZE];

typedef struct pr_sign_s pr_sign_t;
struct pr_sign_s {
    char data[SIGNATURE_SIZE];
    int  size;
};

typedef struct pr_compact_sign_s pr_compact_sign_t;
struct pr_compact_sign_s {
    char data[COMPACT_SIGNATURE_SIZE];
    int  size;
};

typedef pr_compact_sign_t pr_cmpt_t;

typedef struct pr_key_s pr_key_t;
struct pr_key_s {
    pr_pubkey_t  pubkey;
    pr_privkey_t privkey;
};


pr_key_t  *pr_new_key();
pr_sign_t *pr_sign(char *, int, pr_sign_t *, pr_privkey_t *);
int        pr_verify(char *, int, pr_sign_t *, pr_pubkey_t *);
pr_cmpt_t *pr_compact_sign(char *, int, pr_compact_sign_t *, pr_privkey_t *);
int        pr_compact_verify(char *, int, pr_compact_sign_t *, pr_pubkey_t *);
void pr_hash160(const char *src, int size, unsigned char *hash_160);
void pr_hash256(const char *src, int size, unsigned char *hash_256);
#endif

