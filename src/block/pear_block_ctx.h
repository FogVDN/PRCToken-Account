#ifndef _PEAR_BLOCK_CTX_H_
#define _PEAR_BLOCK_CTX_H_

#include "pear_block.h"
#include "store/pear_kv_store.h"

/*
 * 1. Store Tx
 * 2. Store Block And Header
 * 3. Query Block Height
 * 4. Query Header
 * 5.
 */

typedef struct pr_block_ctx_s pr_block_ctx_t;

struct pr_block_ctx_s {
    GList          *blk_list;
    pr_block_t     *current;
    pr_kv_store_t  *hd_store;
    pr_kv_store_t  *tx_store;
    long int        height;
};

typedef struct pr_list_data_s pr_list_data_t;

struct pr_list_data_s {
    pr_block_t    blk;
    unsigned char blk_hash[32];
};


pr_block_ctx_t *pr_new_block_ctx();
pr_block_t     *pr_new_block(pr_block_ctx_t *);

int      pr_store_tx(pr_block_ctx_t *);
int      pr_store_block(pr_block_ctx_t *);
long int pr_query_height(pr_block_ctx_t *);
char    *pr_block_hash(pr_block_ctx_t *, pr_block_t *, unsigned char *hash, int hash_len);
unsigned char *pr_last_block_hash(pr_block_ctx_t *blk_ctx);

#endif
