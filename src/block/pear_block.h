#ifndef _PEAR_BLOCK_H_
#define _PEAR_BLOCK_H_

#include <stdint.h>
#include <glib-2.0/glib.h>

#include "account/pear_addr.h"

/*
 * KV Store Function Cache: List
 * KV Store Function Last:     (HEADER_PREFIX + Block-Index)  =>  decode(header)
 */

#define HEADER_PREFIX "header"

typedef struct pr_block_header_s pr_block_header_t;

struct pr_block_header_s {
    int           timestamp;
    uint64_t      tx_counter;
    uint64_t      total_gas;
    pr_addr_t     coinbase;
    long long int number;
    unsigned char prev_block[32];
    unsigned char merkle_root[32];
    unsigned int  nonce;
};


typedef struct pr_block_s pr_block_t;

struct pr_block_s {
    pr_block_header_t header;
};

#endif


