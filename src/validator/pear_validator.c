#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "log/pear_log.h"
#include "pear_validator.h"
#include "crypto/pear_key.h"


typedef struct pr_validator_s pr_validator_t;

struct pr_validator_s {
    int  power;
    char pubkey[128];
};


pr_validator_ctx_t *pr_new_validator_ctx()
{
    pr_validator_ctx_t *vctx = NULL;

    vctx = (pr_validator_ctx_t *) malloc(sizeof(*vctx));
    if (vctx == NULL)
    {
        PEAR_LOG("malloc validator_ctx error\n");
        return NULL;
    }

    memset(vctx, 0x00, sizeof(*vctx));

    return vctx;
}


int pr_validator_add(pr_validator_ctx_t *valid, char *pubkey, int power)
{
    pr_validator_t *temp = NULL;

    temp = (pr_validator_t *) malloc(sizeof(pr_validator_t));
    if (temp == NULL)
    {
        PEAR_LOG("malloc validator_t error\n");
        return -1;
    }

    temp->power = power;
    strncpy(temp->pubkey, pubkey, strlen(pubkey));

    if ((valid->list = g_list_append(valid->list, temp)) == NULL)
    {
        PEAR_LOG("g_list_append error\n");
        return -1;
    }

    return 1;
}

int pr_validator_del(pr_validator_ctx_t *valid, char *pubkey, int power)
{
    return -1;
}



