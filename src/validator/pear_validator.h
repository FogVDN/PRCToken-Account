#ifndef _PEAR_VALIDATOR_H_
#define _PEAR_VALIDATOR_H_


#include <glib-2.0/glib.h>


typedef struct pr_validator_ctx_s pr_validator_ctx_t;

struct pr_validator_ctx_s {
    int    num;
    GList *list;
};


pr_validator_ctx_t *pr_new_validator_ctx();
int pr_validator_add(pr_validator_ctx_t *, char *, int);
int pr_validator_del(pr_validator_ctx_t *, char *, int);

#endif
