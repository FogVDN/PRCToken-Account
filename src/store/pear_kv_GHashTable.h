#ifndef _PEAR_KV_GHASHTABLE_H_
#define _PEAR_KV_GHASHTABLE_H_

#include "glib.h"
#include "pear_kv_store.h"

typedef struct pr_kv_GHT_s pr_kv_GHT_t;

struct pr_kv_GHT_s {
    pr_kv_store_t handler;
    GHashTable   *table;
};

pr_kv_GHT_t *pr_new_kv_GHT();

#endif
