#ifndef _PEAR_SQLITE_H_
#define _PEAR_SQLITE_H_

#include "sqlite3.h"

typedef char *( *pr_idx_p)(int)  /* Finding the file where the data is located */
typedef char *( *pr_set_p)(void *, void *)  /* Set Data */
typedef char *( *pr_get_p)(void *, void *)  /* Get Data */

struct pr_sqlite_s {
    sqlite3 *db;
    pr_idx_p idx;
    pr_set_p set;
    pr_get_p get;
};


#endif
