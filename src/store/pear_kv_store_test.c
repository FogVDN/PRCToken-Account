#include <stdlib.h>
#include <stdio.h>

#include "pear_kv_store.h"

int main(int argc, char *argv[])
{
    pr_kv_store_t *kvs = pr_new_kv_store(0);
    if (kvs == NULL)
    {
        return -1;
    }

    printf("\n\n#### SET TEST ####\n\n");

    kvs->set(kvs, "abc", "test");
    kvs->set(kvs, "bc", "testdfasfasfasfasdfasdfasfas");


    printf("\n\n#### GET TEST ###\n\n");

    printf("get: %s\n", kvs->get(kvs, "abc"));
    printf("get: %s\n", kvs->get(kvs, "bc"));

    
    printf("\n\n#### GET ERROR TEST ###\n\n");
    printf("get: %s\n", kvs->get(kvs, "jj"));


    printf("\n\n#### GET TEST ###\n\n");
    printf("get: %s\n", kvs->get(kvs, "abc"));
    printf("get: %s\n", kvs->get(kvs, "bc"));


    printf("\n\n#### DEL TEST ####\n\n");

    kvs->del(kvs, "abc");
    kvs->del(kvs, "bc");
    printf("get: %s\n", kvs->get(kvs, "abc"));
    printf("get: %s\n", kvs->get(kvs, "bc"));
}
