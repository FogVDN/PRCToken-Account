#ifndef _PEAR_REDIS_H_
#define _PEAR_REDIS_H_

#include <hiredis.h>
typedef struct pr_redis_s pr_redis_t;

struct pr_redis_s {
    int   port;
    char   host[16];
    struct timeval tv;
    redisContext  *red_ctx;
};

pr_redis_t *pr_new_redis(int, char *, int);
char *pr_redis_get(pr_redis_t *, char *);
int   pr_redis_del(pr_redis_t *, char *);
int   pr_redis_set(pr_redis_t *, char *, char *);
int   pr_redis_upd(pr_redis_t *, char *, char *);

#endif
