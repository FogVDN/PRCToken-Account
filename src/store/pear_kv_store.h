#ifndef _PEAR_KV_STORE_H_
#define _PEAR_KV_STORE_H_


#define KV_REDIS 0
#define KV_GHashTable 1

typedef struct pr_kv_store_s pr_kv_store_t;

typedef char *(*get_p)(pr_kv_store_t *, char *);
typedef char *(*del_p)(pr_kv_store_t *, char *);
typedef char *(*set_p)(pr_kv_store_t *, char *, char *);
typedef char *(*upd_p)(pr_kv_store_t *, char *, char *);

struct pr_kv_store_s {
    get_p get;
    set_p set;
    del_p del;
    upd_p upd;
};

pr_kv_store_t *pr_new_kv_store(char);

#endif

