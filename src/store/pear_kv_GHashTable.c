#include <stdlib.h>

#include "log/pear_log.h"
#include "pear_kv_store.h"
#include "pear_kv_GHashTable.h"


static char *pr_kv_GHT_get(pr_kv_store_t *, char *);
static char *pr_kv_GHT_del(pr_kv_store_t *, char *);
static char *pr_kv_GHT_set(pr_kv_store_t *, char *, char *);
static char *pr_kv_GHT_upd(pr_kv_store_t *, char *, char *);

pr_kv_GHT_t *pr_new_kv_GHT()
{
    pr_kv_GHT_t *kvg = NULL;

    kvg = (pr_kv_GHT_t *) malloc(sizeof(*kvg));
    if (kvg == NULL)
    {
        PEAR_LOG("pr_kv_GHT_t malloc error\n");
        return NULL;
    }

    kvg->handler.get = &pr_kv_GHT_get;
    kvg->handler.set = &pr_kv_GHT_set;
    kvg->handler.del = &pr_kv_GHT_del;
    kvg->handler.upd = &pr_kv_GHT_upd;
}

static char *pr_kv_GHT_get(pr_kv_store_t *kvs, char *key)
{
    pr_kv_GHT_t *kvg = (pr_kv_GHT_t *) kvs;

    return NULL;
}

static char *pr_kv_GHT_set(pr_kv_store_t *kvs, char *key, char *value)
{
    pr_kv_GHT_t *kvg = (pr_kv_GHT_t *) kvs;

    return NULL;
}

static char *pr_kv_GHT_del(pr_kv_store_t *kvs, char *key)
{
    pr_kv_GHT_t *kvg = (pr_kv_GHT_t *) kvs;

    return NULL;
}

static char *pr_kv_GHT_upd(pr_kv_store_t *kvs, char *key, char *value)
{
    pr_kv_GHT_t *kvg = (pr_kv_GHT_t *) kvs;

    return NULL;
}
