#ifndef _PEAR_KV_REDIS_H_
#define _PEAR_KV_REDIS_H_


#include "pear_redis.h"
#include "pear_kv_store.h"

typedef struct pr_kv_redis_s pr_kv_redis_t;

struct pr_kv_redis_s {
    pr_kv_store_t handler;
    pr_redis_t   *redis;
};

pr_kv_redis_t *pr_new_kv_redis();

#endif
