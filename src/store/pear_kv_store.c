#include <stdlib.h>

#include "log/pear_log.h"
#include "pear_kv_store.h"
#include "pear_kv_redis.h"
#include "pear_kv_GHashTable.h"

pr_kv_store_t *pr_new_kv_store(char type)
{
    pr_kv_store_t *kvs = NULL;

    switch(type)
    {
        case 0:
            kvs = (pr_kv_store_t *) pr_new_kv_redis();
            break;

        case 1:
            kvs = (pr_kv_store_t *) pr_new_kv_GHT();
            break;

        default:
            PEAR_LOG("Unknow kv-store Type\n");
            return NULL;
    }

    return kvs;
}

